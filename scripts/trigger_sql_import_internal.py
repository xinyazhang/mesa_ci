import sys
import os
try:
    from urllib2 import urlopen, quote
except:
    from urllib.request import urlopen, quote

import_url = ("http://otc-mesa-ci.jf.intel.com/job/ExportResultsInternal/buildWithParameters?"
              "token=noauth&"
              "url={0}".format(quote(os.environ["BUILD_URL"])))

print("triggering: " + import_url)
urlopen(import_url)

