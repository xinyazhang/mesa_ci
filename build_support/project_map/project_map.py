# Copyright (C) Intel Corp.  2014-2019.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

import sys, os
import xml.etree.cElementTree as et
import pdb

from options import Options

class ProjectMap:
    """provides convenient and consistent access to paths which are
    necessary to the builds"""

    def __init__(self):
        """locate the build specification document, to use as a reference
        point for all other paths"""
        root = os.path.dirname(os.path.abspath(sys.argv[0]))
        if "py.test" in sys.argv[0]:
            root = os.getcwd()
        while True:
            build_spec = root + "/build_specification.xml"
            if not os.path.exists(build_spec):
                if (os.path.dirname(root) == root):
                    # we are at "/"
                    assert(False)
                    return

                root = os.path.dirname(root)
                continue

            # else we have found the spec
            self._source_root = root
            break
        self._internal_source_root = None
        if os.path.exists(os.path.join(self._source_root, root,
                                       "repos/mesa_ci_internal")):
            self._internal_source_root = os.path.join(self._source_root, root,
                                                      "repos/mesa_ci_internal")
        # cache the current_project, because it can't be recalculated
        # if the caller changes directories.
        self._current_project = None
        self._current_project = self.current_project()
        self._merged_build_spec = None

    def source_root(self):
        """top directory, which contains the build_specification.xml"""
        return self._source_root

    def internal_source_root(self):
        """directory which contains the internal build_specification.xml"""
        return self._internal_source_root

    def build_root(self):
        """chroot directory where all results are placed during a build"""
        br = "/tmp/build_root/" + Options().arch
        if not os.path.exists(br):
            os.makedirs(br)
        return br

    def project_build_dir(self, project=None):
        """location of the build.py for the project"""
        if project is None:
            project = self._current_project
        cb = self._source_root + "/" + project + "/"
        return cb

    def project_source_dir(self, project=None):
        """location of the git repo for the project"""
        if project == None:
            project = self.current_project()
        spec = self.build_spec()
        projects_tag = spec.find("projects")
        projects = projects_tag.findall("project")
        for a_project in projects:
            if project != a_project.attrib["name"]:
                continue
            if "src_dir" in a_project.attrib:
                sdir = self._source_root + "/repos/" + a_project.attrib["src_dir"]
                return sdir

        sdir = self._source_root + "/repos/" + project
        if not os.path.exists(sdir):
            return None
        return sdir

    def current_project(self):
        """name of the project which is invoking this method"""
        if self._current_project:
            return self._current_project
        build_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        return os.path.split(build_dir)[1]

    def output_dir(self):
        """logs / test xml go in this directory"""
        o = Options()
        if o.result_path:
            return os.path.abspath(o.result_path + "/test")
        return self._source_root + "/results"

    def _merge_build_spec_element(self, elem_a, elem_b):
        """ Recursively merges elements from elem_b into elem_a. """
        elem_a_sub_tags = [s.tag for s in elem_a]
        for el in elem_b:
            match = None
            if el.attrib.get('name'):
                # select child with same name
                match = elem_a.find(".*[@name='" + el.attrib.get('name')
                                    + "']")
            if el.tag not in elem_a_sub_tags:
                elem_a.append(el)
                continue
            if not len(el):
                if match:
                    # no subs, name matches
                    match = el
                elif el not in elem_a:
                    # no subs, no name
                    elem_a.append(el)
            elif match:
                self._merge_build_spec_element(match, el)
            else:
                next_el = elem_a.findall('./' + el.tag)
                if len(next_el) > 1:
                    # unique element since no match by name
                    elem_a.append(el)
                    continue
                self._merge_build_spec_element(next_el[0], el)

    def _merge_build_specs(self, build_specs):
        """ merge any number of xml trees listed in build_specs into the first
            tree in the list and returns the first tree in the list """
        xml_roots = [xml.getroot() for xml in build_specs]
        # loop through and merge subsequent roots with the first root
        for root in xml_roots[1:]:
            self._merge_build_spec_element(xml_roots[0], root)
        return build_specs[0]

    def build_spec(self):
        if not self._merged_build_spec:
            external_bs = et.parse(self.source_root()
                                   + "/build_specification.xml")
            if (not self.internal_source_root()
                    or not os.path.exists(self.internal_source_root()
                                          + "/build_specification.xml")):
                self._merged_build_spec = external_bs
                return self._merged_build_spec
            internal_bs = et.parse(self.internal_source_root()
                                   + "/build_specification.xml")
            self._merged_build_spec = self._merge_build_specs([external_bs,
                                                              internal_bs])
        return self._merged_build_spec
