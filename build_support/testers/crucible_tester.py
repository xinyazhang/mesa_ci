import os
import os.path as path
import xml.etree.ElementTree as ET
import configparser
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet
from utils.command import run_batch_command
from export.export import Export
from utils.utils import get_conf_file, NoConfigFile, mesa_version
from utils.check_gpu_hang import check_gpu_hang
from bisect_test import TestLister


def post_process_results(xml):
    t = ET.parse(xml)
    o = Options()
    conf = None
    long_revisions = RepoSet().branch_missing_revisions()
    missing_revisions = [a_rev[:6] for a_rev in long_revisions]
    try:
        conf = get_conf_file(o.hardware, o.arch,
                             project=ProjectMap().current_project())
    except NoConfigFile:
        pass
    if conf:
        # key=name, value=status
        expected_status = {}
        changed_commit = {}
        c = configparser.SafeConfigParser(allow_no_value=True)
        c.read(conf)
        for section in c.sections():
            for (test, commit) in c.items(section):
                if test in expected_status:
                    raise Exception("test has multiple entries: " + test)
                expected_status[test] = section
                changed_commit[test] = commit
        testsuite = t.find('./testsuite')
        for atest in testsuite.findall(".//testcase"):
            test_name = atest.attrib["name"].lower()
            if atest.attrib["status"] == "lost":
                atest.attrib["status"] = "crash"
            if atest.attrib["status"] == "skip":
                testsuite.remove(atest)
                continue
            if test_name not in expected_status:
                continue

            expected = expected_status[test_name]
            test_is_stale = False
            for missing_commit in missing_revisions:
                if missing_commit in changed_commit[test_name]:
                    test_is_stale = True
                    # change stale test status to skip
                    for ftag in atest.findall("failure"):
                        atest.remove(ftag)
                    for ftag in atest.findall("error"):
                        atest.remove(ftag)
                    atest.append(ET.Element("skipped"))
                    so = ET.Element("system-out")
                    so.text = ("WARN: the results of this were changed by "
                               + changed_commit[test_name])
                    so.text += ", which is missing from this build."
                    atest.append(so)
                    break
            if test_is_stale:
                continue

            if expected == "expected-failures":
                # change fail to pass
                if atest.attrib["status"] == "fail":
                    for ftag in atest.findall("failure"):
                        atest.remove(ftag)
                    so = ET.Element("system-out")
                    so.text = "Passing test as an expected failure"
                    atest.append(so)
                elif atest.attrib["status"] == "crash":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = ("ERROR: this test crashed when it expected "
                               + "failure")
                    atest.append(so)
                elif atest.attrib["status"] == "pass":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test passed when it expected failure"
                    atest.append(so)
                elif atest.attrib["status"] == "skip":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = ("ERROR: this test skipped when it expected "
                               + "failure")
                    atest.append(so)
                else:
                    raise Exception("test has unknown status: "
                                    + atest.attrib["name"]
                                    + " " + atest.attrib["status"])
            elif expected == "expected-crashes":
                # change error to pass
                if atest.attrib["status"] == "crash":
                    for ftag in atest.findall("error"):
                        atest.remove(ftag)
                    so = ET.Element("system-out")
                    so.text = "Passing test as an expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "fail":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test failed when it expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "pass":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test passed when it expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "skip":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test skipped when it expected crash"
                    atest.append(so)
                else:
                    raise Exception("test has unknown status: "
                                    + atest.attrib["name"]
                                    + " " + atest.attrib["status"])

    for atest in t.findall(".//testcase"):
        atest.attrib["name"] = atest.attrib["name"] + "." + o.hardware + o.arch
    t.write(xml)


class CrucibleTester(object):
    def __init__(self):
        self.blacklist = None

    def build(self):
        pass

    def clean(self):
        pass

    def test(self):
        pm = ProjectMap()
        build_root = pm.build_root()
        global_opts = Options()
        if global_opts.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
        elif global_opts.arch == "m32":
            icd_name = "intel_icd.i686.json"
        env = {"LD_LIBRARY_PATH": build_root + "/lib",
               "VK_ICD_FILENAMES": (build_root + "/share/vulkan/icd.d/"
                                    + icd_name),
               "ANV_ABORT_ON_DEVICE_LOSS": "true"}
        o = Options()
        o.update_env(env)
        br = ProjectMap().build_root()
        out_dir = br + "/../test"
        if not path.exists(out_dir):
            os.makedirs(out_dir)
        out_xml = (out_dir + "/piglit-crucible_" + o.hardware
                   + "_" + o.arch + ".xml")
        include_tests = []
        if o.retest_path:
            include_tests = TestLister(o.retest_path + "/test/").RetestIncludes("crucible-test")

        conf_dir = pm.source_root() + "/crucible-test/"
        blacklist = [line.rstrip() for line in open(conf_dir
                                                    + "blacklist.conf")]
        if o.arch == "m32":
            blacklist = [line.rstrip() for line in open(conf_dir + "m32_blacklist.conf")]

        platform_blacklist = conf_dir + o.hardware + "_blacklist.conf"
        if os.path.exists(platform_blacklist):
            blacklist += [line.rstrip() for line in open(platform_blacklist)]

        internal_conf_dir = (pm.source_root()
                             + "/repos/mesa_ci_internal/crucible-test")
        internal_blacklist = internal_conf_dir + "blacklist.conf"
        if os.path.exists(internal_blacklist):
            blacklist += [line.rstrip() for line in open(internal_blacklist)]
        internal_platform_blacklist = (internal_conf_dir + o.hardware
                                       + "_blacklist.conf")
        if os.path.exists(internal_platform_blacklist):
            blacklist += [line.rstrip() for line in open(internal_platform_blacklist)]

        # remove commented lines from blacklist and prepend !
        blacklist = [('!' + b) for b in blacklist if (not b.startswith('#') and b)]

        parallelism = []

        if "ivb" in o.hardware:
            parallelism = ['-j', '1']

        if "byt" in o.hardware:
            parallelism = ['-j', '1']

        if "icl" in o.hardware:
            if "18.2" in mesa_version():
                # ICL not supported
                return
        options = parallelism + include_tests + blacklist
        run_batch_command([br + "/bin/crucible",
                           "run", "--fork", "--log-pids",
                           "--junit-xml=" + out_xml] + options,
                          env=env,
                          expected_return_code=None)
        post_process_results(out_xml)
        run_batch_command(["cp", "-a", "-n",
                           out_dir, pm.source_root()])

        check_gpu_hang()
        Export().export_tests()
