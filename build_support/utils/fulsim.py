#!/usr/bin/env python3
import glob
import os
import sys
from options import Options
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


class Fulsim(object):

    def __init__(self):
        # key_file - a file under the mesa repo for determining whether or not
        # the mesa branch supports this platform
        self.platform_keyfile = {
            'tgl_sim': 'src/intel/genxml/gen12.xml',
        }
        if internal_vars:
            self.platform_keyfile.update(internal_vars.platform_keyfile)
        self._hardware = Options().hardware.replace('_iris', '')
        self._arch = Options().arch
        self._project_map = ProjectMap()
        self._build_root = self._project_map.build_root()
        self._mesa_repo_dir = self._project_map.source_root() + "/repos/mesa/"

    def is_supported(self):
        """ Determines if the hardware is supported for running on fulsim """
        if self._hardware not in self.platform_keyfile:
            print("Environment check failure: Unable to find the expected "
                  "fulsim version for this platform.")
            return False
        # Check for existence of the keyfile if there is one
        if self.platform_keyfile[self._hardware]:
            if not os.path.exists(self._mesa_repo_dir + "/"
                                  + self.platform_keyfile[self._hardware]):
                print("Environment check failure: An unsupported Mesa version "
                      "was detected. Make sure the correct Mesa sha was "
                      "specified.")
                return False
        # 32-bit only
        if self._arch == "m32":
            print("Environment check failure: 32-bit is not supported.")
            return False
        return True

    def get_env(self):
        env = {}
        # Additional configuration if running in simulation
        if self._hardware in self.platform_keyfile:
            simdrm_lib_path = glob.glob(os.path.join(self._build_root,
                                                     'lib/**/site-packages'))
            if not simdrm_lib_path or not os.path.exists(simdrm_lib_path[0]):
                raise Exception("ERROR: unable to find sim_drm.py")
            simdrm_lib_path = simdrm_lib_path[0]
            sys.path.append(simdrm_lib_path)
            import sim_drm
            aubload = os.path.expanduser(self._build_root
                                         + '/opt/fulsim/' + self._hardware
                                         + '/AubLoad')
            env = sim_drm.get_env(self._hardware.split('_sim')[0],
                                  aubload=aubload,
                                  libsim_drm=(self._build_root
                                              + '/lib/libsim-drm.so'))
        return env
