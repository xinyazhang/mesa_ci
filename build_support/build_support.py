import os
import shutil
import socket
import subprocess
import sys
import time
from utils.check_gpu_hang import check_gpu_hang
from utils.command import rmtree, run_batch_command
from export import Export
from utils.utils import (DefaultTimeout, NullInvoke, get_libdir,
                         get_libgl_drivers)
from utils.timer import TimeOut
from options import Options
from project_invoke import ProjectInvoke
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


def build(builder, options=None, time_limit=None, import_build=True):

    if not time_limit:
        time_limit = DefaultTimeout()
    if not options:
        options = Options()
    action_map = [
        ("clean", builder.clean),
        ("build", builder.build),
        ("test", builder.test),
    ]
    actions = options.action

    invoke = NullInvoke()

    if "PKG_CONFIG_PATH" in os.environ:
        del os.environ["PKG_CONFIG_PATH"]
    if "LD_LIBRARY_PATH" in os.environ:
        del os.environ["LD_LIBRARY_PATH"]
    if "LIBGL_DRIVERS_PATH" in os.environ:
        del os.environ["LIBGL_DRIVERS_PATH"]

    # TODO: add this stuff
    if (options.result_path):
        # if we aren't posting to a server, don't attempt to write
        # status
        invoke = ProjectInvoke(options)

    invoke.set_info("start_time", time.time())

    if options.hardware != "builder" and check_gpu_hang():
        return

    # start a thread to limit the run-time of the build
    to = TimeOut(time_limit, invoke=invoke)
    to.start()

    exporter = Export()
    if import_build:
        exporter.import_build_root()

    if type(actions) is str:
        actions = [actions]

    # clean out the test results directory, so jenkins processes only
    # the files for the current build
    if "test" in actions:
        test_out_dir = ProjectMap().source_root() + "/test"
        if os.path.exists(test_out_dir):
            rmtree(test_out_dir)

    # Walk through the possible actions in order, if those actions are not
    # requested go on. The order does matter.

    for k, a in action_map:
        if k not in actions:
            continue
        options.action = a

        # Mapping of platform hardware type to string used for searching
        # i965_pci_ids.h for support
        sim_pciids_name = {
            'tgl_sim': 'Tigerlake',
        }
        if internal_vars:
            sim_pciids_name.update(internal_vars.sim_pciids_name)
        # Note: this check for supported gpu should be skipped when 'building'
        # fulsim since it doesn't depend on the 'mesa' project and will almost
        # certainly fail
        if (k == "test" and options.hardware != "builder"
                and ProjectMap().current_project() != "fulsim"):
            hostname = socket.gethostname()
            # Make sure that simulated platforms are supported in the mesa
            # being tested
            if '_sim' in options.hardware:
                pciid_name = None
                if options.hardware in sim_pciids_name:
                    pciid_name = sim_pciids_name[options.hardware]
                try:
                    if not pciid_name:
                        raise
                    file_names = ['i965_pci_ids.h', 'iris_pci_ids.h']
                    mesa_pciids_dir = (ProjectMap().source_root()
                                       + '/repos/mesa/include/pci_ids/')

                    if not os.path.exists(mesa_pciids_dir):
                        raise
                    found = False
                    for file_name in file_names:
                        mesa_pciids_file = mesa_pciids_dir + file_name
                        try:
                            with open(mesa_pciids_file, 'r') as f:
                                for line in f.readlines():
                                    if pciid_name in line:
                                        found = True
                                        break
                        except FileNotFoundError:
                            continue
                        finally:
                            if not found:
                                print('pciid not found, trying another file')

                        if found:
                            break

                    if not found:
                        raise
                except Exception:
                    Export().create_failing_test("unsupported-gpu-pciid-"
                                                 + hostname,
                                                 "This gpu is not supported "
                                                 + "in the Mesa under test.")
                    to.end()
                    sys.exit("This gpu is not supported in the Mesa under "
                             + "test.")
            else:
                # make sure not using llvm pipe
                br = ProjectMap().build_root()
                wflinfo = br + "/bin/wflinfo"
                env = {}
                env.update({
                    'LD_LIBRARY_PATH': ':'.join([
                        get_libdir(),
                        get_libgl_drivers(),
                    ]),
                    "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
                })

                try:
                    (out, _) = run_batch_command([wflinfo,
                                                  "--platform=gbm", "-a",
                                                  "gl"], streamedOutput=False,
                                                 env=env)
                    if "renderer string: llvmpipe" in out.decode():
                        invoke.set_info("end_time", time.time())
                        invoke.set_info("status", "failed")

                        fail_out = out.decode()

                        Export().create_failing_test("llvmpipe-detected-"
                                                     + hostname,
                                                     fail_out)
                        print("ERROR: build aborted")
                        to.end()
                        sys.exit("llvmpipe software rendering detected. "
                                 "Aborting test.")
                except AssertionError as e:
                    print(e)
                    print("WARN: skipping llvmpipe check.")
                except subprocess.CalledProcessError as e:
                    print(e)
                    print("ERROR: wflinfo failed")
                    Export().create_failing_test("wflinfo-crash-"
                                                 + hostname,
                                                 "wflinfo crashed!")
                    to.end()
                    sys.exit(1)

        if k == "test" and "CACHE_DISABLE" in options.env:
            shader_cache_test = True
        else:
            shader_cache_test = False
        try:
            if shader_cache_test:
                print("Deleting shader cache")
                cache_dir = os.path.expanduser("~/.cache/mesa_shader_cache")
                if os.path.exists(cache_dir):
                    shutil.rmtree(cache_dir)
            a()
            if shader_cache_test:
                print("Running a second time to test shader cache.")
                a()
        except:
            # we need to cancel the timer first, in case
            # set_status fails, and the timer is left running
            to.end()
            invoke.set_info("status", "failed")
            # must cancel timeout timer, which will prevent process from ending
            raise

    if import_build:
        # tidy up the temporary build root that was used to identify
        # new files
        exporter.clean_orig_build_root()

    # must cancel timeout timer, which will prevent process from
    # ending.  cancel the timer first, in case set_status fails, and
    # the timer is left running
    to.end()
    print("build finished")
    invoke.set_info("end_time", time.time())
    invoke.set_info("status", "success")
